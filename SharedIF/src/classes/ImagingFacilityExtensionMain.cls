/**************************************************************************************************************************************
Name: ImagingFacilityExtensionMain
Copyright © BlueStar Infotech
=======================================================================================================================================
Purpose: To Create/Edit details of Imaging facility object .
=======================================================================================================================================
History
-----------------------------------------------------------------------------------------------------------------------------------
VERSION        AUTHOR                       DATE              DETAIL                                                        Ticket #
1.0                                                           Created
2.0            Krishna Kurava               09-May-2016       If IF is shared, Phantom Scan In-Vivo qualification           SAL-294
															  is waived. The Requested or received Date is not applicable.
2.1			   Rajeshwari Padmasali		    09-May-2016		  Identify which IF is shared with which Study Site(s)     		SAL-286															 

**************************************************************************************************************************************/ 

public class ImagingFacilityExtensionMain {
        
        // boolean properties
    public boolean haveOff {get;set;}
    public boolean haveAcc {get;set;}    
    public boolean ptq_sent {get;set;}
    public boolean ptq_rec {get;set;}    
    public boolean haveSupContact {get;set;}
    public boolean sup_sent {get;set;}
    public boolean testrun_req {get;set;}
    public boolean testrun_rec {get;set;}
    public boolean trainingExists {get;set;}
    public boolean techExists {get;set;}
    public boolean inv_sent {get;set;}
    public boolean mri {get;set;}
    public boolean dxa {get;set;}
    public boolean neuro {get;set;}  
    public boolean PET {get;set;}        
    public boolean invivo_required {get;set;}
    public boolean noPTQversion {get;set;}

    
    public boolean showResults {get;set;}    
    public List<SharedImagingFacilityWrapper.ImagingFacilityLookupWrapper>  imfWrapList 		{get;set;}// Wrapper List of all Related Imaging facility Record
	public Imaging_Facility__c oImg 															{get;set;}//To store current Imaging Facility Record
	public String sharedIf1 																		{get;set;}// To Store Selected Imaging Facility as Primary
	public static Integer sharedIFCount {get;set;}
	public boolean dispList {get;set;}
	public String DXAemailTemp {get;set;}
	public boolean dispRec {get;set;}
           
    public void init_booleans (){
        if (this.imgfac.office__c != null){     haveOff = true; }
        if (this.imgfac.account__c != null){ haveAcc = true; }          
        if (this.imgfac.PTQ_sent__c != null){ ptq_sent = true;  }
        if (this.imgfac.PTQ_received__c != null){ ptq_rec = true; }
        if (this.imgfac.supply_contact__c != null){ haveSupContact = true; }
        if (this.imgfac.supplies_sent__c != null){ sup_sent = true; }
        if (this.imgfac.test_run_requested__c != null){ testrun_req = true; }
        if (this.imgfac.test_run_received__c != null){ testrun_rec = true; }
        if (this.imgfac.Total_Training_Sessions__c > 0){ trainingExists = true; }
        if (this.imgfac.Count_Primary_Techs__c > 0){ techExists = true; }
        if (this.imgfac.Training_Invitation_Sent__c != null){ inv_sent = true; }
        if (this.imgfac.modality__c == 'MRI'){ mri = true; }
        if (this.imgfac.modality__c == 'PET'){ PET = true; }
        if (this.imgfac.modality__c == 'DXA'){ dxa = true; }        

    
        if (this.imgfac.study__r.therapeutic_area__c == 'Neurology'){ neuro = true; }
                if (this.imgfac.study__r.in_vivo_qualification__c == true && (this.imgfac.modality__c == 'MRI' || this.imgfac.modality__c == 'PET')){
                        invivo_required = true;
        } else {
                invivo_required = false;
                }
                
        // check for existing ptq version
        list<ptq_version__c> ptqs = new list<ptq_version__c>();
        ptqs = [select id from ptq_version__c where study__c = : this.imgfac.study__c and modality__c = :this.imgfac.modality__c];
        if (ptqs.size() < 1){
            noPTQversion = true;
        }  
    }           
       
    private ApexPages.StandardController controller;
    private final imaging_facility__c imgfac;
    
    public contact sc {get;set;}
    public contact tech {get;set;}   
    public string  SharedIF {get;set;}         
    
    public ImagingFacilityExtensionMain(ApexPages.StandardController stdController) {
        controller = stdController;
        sharedIFCount = 0;
        dispList = false;
        dispRec = false;
        Email_Template__c et = new Email_Template__c();
        et = Email_Template__c.getValues('DXA');
        DXAemailTemp = et.Id__c;
        system.debug('DXAemailTemp -->'+DXAemailTemp);
        
        this.imgfac = (imaging_facility__c)controller.getrecord();
                init_booleans();   
                     
        sc = new contact();        
        if (this.imgfac.study_site_sc__c != null){
                sc = [select id, name, email from contact where id = :this.imgfac.study_site_sc__c];
        }                
        tech = new contact();	
        if (this.imgfac.primary_technologist__c != null){
                tech = [select id, name, email from contact where id = :this.imgfac.primary_technologist__c];           
        }   
        
        SharedIF = ApexPages.currentPage().getparameters().get('SharedIf');
        if(SharedIF !=null && SharedIF!=''){
        	this.imgfac.Shared__c = true;
        	this.imgfac.Shared_Imaging_Facility__c = SharedIF;
        	this.imgfac.Shared_Type__c = 'Secondary';
        }     
        
        imfWrapList = new List<SharedImagingFacilityWrapper.ImagingFacilityLookupWrapper> ();
	    oImg = new Imaging_Facility__c();
	    String imgId = System.currentPageReference().getParameters().get('id');
	    //Retrives current Imaging facility Record
        imaging_facility__c imgFac = [select id, study__C, shared__C, Shared_Type__c, Modality__C,  Shared_Imaging_Facility__c from imaging_facility__c where id = : ImgId];
	    
	    this.imgfac.id = imgFac.id;
	    this.imgfac.Study__C = imgFac.Study__C;
	    this.imgfac.Shared_Imaging_Facility__c = imgfac.Shared_Imaging_Facility__c;
	    this.imgfac.Modality__C =imgfac.Modality__C;
	    this.imgfac.Shared_Type__c =imgfac.Shared_Type__c;
	    //Retrives all shared related Imaging facility Record wrt current Imaging facility Record and Assign to wrapper List
	    String limitSize = '5'; 
	    imfWrapList = SharedImagingFacilityHelper.AssignImgFacToWrapper(limitSize.trim(),this.imgfac);// 
	    if(sharedIFCount > 5){
	    	dispList = true;
		}
		if(imfWrapList.size() > 0){
			dispRec = true;
		}
	    system.debug('RelatedSharedImagingFacility Ends here  -->'+imfWrapList); 
    }
    
    list<training_session__c> completed_trainings;
    public list<authorized_technologist__c> authsToInsert {get;set;}    
    set<id> AuthorizedContacts;
    list<equipment_evaluation__c> approvedScanners;
              
    public string total_new {get;set;}
    
    public map<id,date> at_map = new map<id,date>();
    
    public set<id> getAuthorizedContacts(){
        AuthorizedContacts = new set<id>();
        
        // list of existing authorized techs already in the database
        list<authorized_technologist__c> authorizedTechs = 
                [select id, contact__c, training_session__r.completed_date__c from authorized_technologist__c where imaging_facility__c = :this.imgfac.id];
                
        // geterate set of contact ids for existing list of authorized techs
        if(authorizedTechs.size() > 0){
                for (authorized_technologist__c at : authorizedTechs){
                        AuthorizedContacts.add(at.contact__c);                                                  
                }
        }
        return AuthorizedContacts;
    }
    
    public list<training_session__c> getCompleted_Trainings(){
        completed_trainings =  new list<training_session__c>();
        //query for related training sessions and attendees where a training date exists and status is Completed or Waived
        completed_trainings = [select id, completed_date__c, training_format__c,
                (select id, contact__c, training_session__r.completed_date__c, training_session__r.training_format__c, 
                training_sign_off_received__c, training_session__r.status__c from attendees__r where role__c = 'Technologist')
                from training_session__c
            where completed_date__c != null
            and (status__c = 'Completed' or status__c = 'Waived')
            and imaging_facility__c = :this.imgfac.id
            order by completed_date__c];
            
        return completed_trainings;        
    }
                
    public list<equipment_evaluation__c> getApprovedScanners(){
        approvedScanners = new list<equipment_evaluation__c>();
        
        ApprovedScanners = [select id, status__c, equipment_review_date__c from equipment_evaluation__c
            where imaging_facility__c = : this.imgfac.id
            and (status__c ='Waived' or status__c = 'Accepted')
            order by equipment_review_date__c asc];        
        
        return approvedscanners;
    }                 
    
    public boolean checkInvivo(imaging_facility__c i){
        boolean proceed;
        if (invivo_required && i.in_vivo_qualification_status__c != 'Accepted'){
                proceed = false;
        } else {
                proceed = true;
        }
        return proceed;
    }            
    
    public map<id,attendee__c> con_att_map = new map<id,attendee__c>();
    
    public pageReference authorizeTechs(){
                authsToInsert = new list<authorized_technologist__c>();
                
        getApprovedScanners();
        getCompleted_Trainings();
        getAuthorizedContacts();                
                                   
        // only proceed if there is at least one approved scanner. if in vivo is required, its status must be 'Accepted'
        // loop through training sessions and attendees to populate map of contact id + attendee sobject;

        if ((approvedScanners.size() > 0 )|| this.imgfac.No_Test_Data_Required_per_Sponsor__c == true){  //&& checkInvivo(this.imgfac)
            for (training_session__c t :completed_trainings){
                SYSTEM.DEBUG('- - - - - - - - - - MADE IT THIS FAR - - - - - - - - - - ');                                                                                                                                                
                for (attendee__c a : t.attendees__r){                                                                                                                                                                           
                    // if already in map and training date is earlier, update value in map
                    if (con_att_map.containskey(a.contact__c) &&
                          t.completed_date__c < con_att_map.get(a.contact__c).training_session__r.completed_date__c){
                          con_att_map.put(a.contact__c,a);
                    } else {
                        // put contact id and attendee in map for first time
                                                if (authorizedContacts.contains(a.contact__c) == false){
                                con_att_map.put(a.contact__c,a);
                            }
                    }
                }
                        } // end outside for loop
                
            //iterate through map and construct an authTech for each contact id. add to list of auths for insertion
                        for (id con :con_att_map.keyset()){
                                authorized_technologist__c authTech = new authorized_technologist__c();
                        authTech.contact__c = con;
                authTech.imaging_facility__c = this.imgfac.id;
                authTech.training_session__c = con_att_map.get(con).training_session__c;
                authTech.Authorization_Date__c = date.today();
                authsToInsert.add(authTech);                                                                    
            }
                                                                                                                        
            try{                
                if (authsToInsert.size()>0){
                    total_new = String.ValueOf(authsToInsert.size());                                                                              
                    insert authsToInsert;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.confirm, total_new + ' technologists authorized.'));
                    if(this.imgfac.date_authorized__c == null){
                        this.imgfac.date_authorized__c = date.today();
                        update this.imgfac;
                    }
                    showResults = true;
                }
            }catch (DMLexception e){
                ApexPages.addMessages(e);
            }
        }
        
        // Begin Page Messages        
       /* if(Completed_Trainings.size() < 0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, '0 technologists authorized. There are no completed training sessions.'));                                 
        } else if (checkInvivo(this.imgfac) == false && this.imgfac.No_Test_Data_Required_per_Sponsor__c == false ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, '0 technologists authorized. In Vivo Qualification is still pending.'));                        
        } else*/ if (approvedScanners.size() < 1  && this.imgfac.No_Test_Data_Required_per_Sponsor__c == false){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, '0 technologists authorized. An Approved/Waived ' + this.imgfac.test_run_type__c +' does not exist.'));                                                    
        } else if  (authsToInsert.size() < 1){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.warning, '0 technologists authorized. There are no new technologists to authorize.'));
            
        } 
        return Page.ImgFacAuthResults;
    }
                        
    public pagereference goBack(){
        PageReference ImgFacPage = new ApexPages.StandardController(this.imgfac).view();
        ImgFacPage.setRedirect(true);
        return ImgFacPage;
    }
    
    public pagereference save(){
     
     List<imaging_facility__c> imgList = new List<imaging_facility__c>();
     //Krishna- Added try catch block to handling the exception for SAL-294
     try{ 
    	    //Updating the record  - Commenetd below code as part of Shared Imaging facility - Raji SAL-286 - Starts here
    	    System.debug('this.imgfac.Shared_Imaging_Facility__c **'+this.imgfac);
    	    if(this.imgfac.Shared__c == false){
	    		this.imgfac.Shared_Imaging_Facility__c = null;
	    		this.imgfac.Shared_Type__c = null;
	    	 }else if(this.imgfac.Shared_Imaging_Facility__c!= null){
	    	 	//Update this.imgfac.Shared_Imaging_Facility__c as Primary
	    	 	 System.debug('this.imgfac.Shared_Imaging_Facility__c ** imgList'+ this.imgfac);
	    	 	imaging_facility__c imgPrimary = new imaging_facility__c();
	    	 	imgPrimary.id = this.imgfac.Shared_Imaging_Facility__c;
	    	 	imgPrimary.Shared__c = true;
	    	 	imgPrimary.Shared_Type__c = 'Primary';
	    	 	//Current Imaging Facility as Secondary
	    	 	this.imgfac.Shared_Type__c = 'Secondary';
	    	 	imgList.add(imgPrimary);
	    	 }else if(this.imgfac.Shared__c == true && ( this.imgfac.Shared_Type__c ==null || (this.imgfac.Shared_Type__c !=null && this.imgfac.Shared_Type__c !='Primary')  )){
	    	 	 ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.error, 'Please select the Primary Site'));
	    	 	 return null;
	    	 }
	    	System.debug('this.imgfac.Shared_Imaging_Facility__c ** imgList'+imgList);
    	    imgList.add(this.imgfac);
    	    update imgList;
    	    //Updating the record  - Commenetd below code as part of Shared Imaging facility - Raji SAL-286 - Ends here 
	    	PageReference ImgFacPage = new ApexPages.StandardController(this.imgfac).view();
	        ImgFacPage.setRedirect(true);           
	        return ImgFacPage;
    	 }//Krishna- catch block starts SAL-294
    	 catch (DMLexception e){
                ApexPages.addMessages(e);
         }      
	  
    return null;  
   }
   public pagereference checkMessages(){
        if (sc.id == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.warning, 'A SC should be assigned to the study site before completing any authorization activities.'));   
        } else if (sc.email == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.warning, 'Email for SC ' + sc.name + ' is blank.'));
        }
        if(tech.id != null && tech.email == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.warning, 'Email for Tech ' + tech.name + ' is blank.'));
        }
        return null;
    }
    
     /******************************************************************************************
        Purpose     : Method to redirect for Imaging facility record
        Parameters  : imgfac id
        Returns     : Return the Img facility records. 
    ******************************************************************************************/
     public pagereference SharedImgFacLookUp(){
        PageReference ImgFacPage = new PageReference('/apex/ImgFacCustomLookUp?id='+imgfac.id);
        ImgFacPage.setRedirect(true);           
        return ImgFacPage;
    }
    
     /******************************************************************************************
        Purpose     : Method to redirect for Imaging facility record
        Parameters  : imgfac id
        Returns     : Return the Img facility records. 
    ******************************************************************************************/
     public pagereference ViewRelatedSharedImgFac(){
        PageReference ImgFacPage = new PageReference('/apex/SharedImagingFacility?id='+imgfac.id);
        ImgFacPage.setRedirect(true);           
        return ImgFacPage;
    }
     
     /******************************************************************************************
        Purpose     : Validate for waived status.
        Parameters  : imgfac id
        Returns     : Return error message. 
    ******************************************************************************************/
     public pagereference WaivedCheck(){
     	system.debug('WaivedCheck Starts here  -->'); 
     	List<Imaging_Facility__c> imgFacList = [Select i.Shared__C,  i.Training_Status__c, i.Test_Run_Results__c, i.Retest_Status__c, i.Modality__c, i.In_Vivo_Qualification_Status__c, i.Id From Imaging_Facility__c i where i.id =:this.imgfac.id];
	  	system.debug('imgFacList -->'+imgFacList);  
	  	String error = '';
	  	Boolean errflag = false;
	  	if(!imgFacList.isempty() && imgFacList[0].Shared__C ){
	  		if(imgFacList[0].Modality__c == 'MRI' && imgFacList[0].Test_Run_Results__c =='Waived'){
	  			error = 'Phantom Scan';
	  		}
	  		else if(imgFacList[0].Modality__c == 'DXA' && imgFacList[0].Test_Run_Results__c =='Waived'){
	  			error = 'Baseline Qualification ';
	  		}else if(imgFacList[0].Test_Run_Results__c =='Waived'){
	  			error = 'Test Run';
	  		}
	  		if(error !='' && error !=null){
	  			this.imgfac.Shared__C = true;
	  			errFlag = true;
	  			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to update. The below activities are still waived and will need to be updated: '));
	  			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, error));
	  		}
	  		if(imgFacList[0].Training_Status__c =='Waived'){
	  			this.imgfac.Shared__C = true;
	  			if(!errFlag){
	  				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to update. The below activities are still waived and will need to be updated: '));
	  			}
	  			errFlag = true;
	  			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Technologist Training'));
	  		}
	  		
	  		if(imgFacList[0].In_Vivo_Qualification_Status__c =='Waived'){
	  			this.imgfac.Shared__C = true;
	  			if(!errFlag){
	  				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to update. The below activities are still waived and will need to be updated: '));
	  			}
	  			errFlag = true;
	  			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'In-Vivo Qualification'));
	  		}
	  		
	  		if(imgFacList[0].Retest_Status__c =='Waived'){
	  			this.imgfac.Shared__C = true;
	  			if(!errFlag){
	  				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to update. The below activities are still waived and will need to be updated: '));
	  			}
	  			errFlag = true;
	  			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Retest'));
	  		}
	  		if(error!='' && error!=null){
	  			this.imgfac.Shared__C = true;
	  			return null;
	  		}
	  	}
	  	PageReference imgFacPage = new PageReference('/apex/ImgFacView?id='+ this.imgfac.id);
        imgFacPage.setRedirect(true); 
        system.debug('ViewImgFac Ends here  -->');           
        return null;
     }
     
}