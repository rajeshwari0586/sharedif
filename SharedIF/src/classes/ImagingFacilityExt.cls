public class ImagingFacilityExt {
        
    public String ssID                                   {get;set;}
    public Study_Site__c ss                              {get;set;}
    public study__c s                                    {get;set;}
    public String Mod                                    {get;set;}
    public String[] sMods                                {get;set;}
    public boolean noMods                                {get;set;}
    public boolean noStudySite                           {get;set;}
    private ApexPages.StandardController controller;
    private final imaging_facility__c imgfac;    
    
    // constructor    
    public ImagingFacilityExt(ApexPages.StandardController stdController) {
        controller = stdController;
        this.imgfac = (imaging_facility__c)controller.getrecord();
        ssID = ApexPages.currentPage().getParameters().get('ssID');
        if (ssID != null && ssID != ''){
            noStudySite = false;
            ss = [select id, name, modality__c, study__c from study_site__c where id =:ssID];
            s = [select id, name, modalities__c from study__c where id = :ss.study__c];
        
        this.imgfac.study_site__c = ss.id;
        this.imgfac.study__c = s.id;
        user u = [select loc__c from user where id = :userinfo.getuserid()];
        this.imgfac.office__c = u.loc__c; 
        if (s.modalities__c != null){
            sMods = s.modalities__c.split(';');
            noMods = false;
            } else {noMods = true;}
        } else {noStudySite = true; noMods=true;}
    }           
       
    
//Modality Methods
    public List<SelectOption> getModalities() {
        List<SelectOption> modOptions = new List<SelectOption>();
        if (sMods != null){
            for(String m : sMods){
               modOptions.add(new selectOption(m, m));
            }
        }
        if (modOptions.size() == 0){noMods = true;}                    
        return modOptions;
    }
    
    public void setMod(String M) {
        Mod = M;
    }
    
    public pageReference Save(){
        try{
            insert(this.imgfac);
            pagereference p = new pagereference('/'+ this.imgfac.id);
            return p;
        } catch (DMLexception e) {
            apexpages.addmessages(e);
        }
        return null;
    }
        
}