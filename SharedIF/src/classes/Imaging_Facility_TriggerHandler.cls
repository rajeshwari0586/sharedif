/***********************************************************************************************************************************************
Name: Imaging_Facility_TriggerHandler
Copyright © Bioclinica
================================================================================================================================================
================================================================================================================================================
Purpose: Helper Class for Imaging_Facility_Trigger
================================================================================================================================================
================================================================================================================================================
History
-------
VERSION        AUTHOR                             DATE              DETAIL          
1.0    		   Bioclinica
2.0            Rajeshwari Padmasali               21-Mar-2016       Helper class for Imaging Facility Trigger - Modified this class SAL 326
************************************************************************************************************************************************/
public without sharing class Imaging_Facility_TriggerHandler {
	public static void updateStudyFacilityCount(Map<Id, Imaging_Facility__c> imagFacMap) {
		Set<Id> studyIds = new Set<Id>();
		Map<Id, Integer> studyMapToSupplied = new Map<Id, Integer>();
		Map<Id, Integer> studyMapToNotSupplied = new Map<Id, Integer>();
		Map<Id, Integer> studyMapToInactive = new Map<Id, Integer>();

		for(Imaging_Facility__c ifr : imagFacMap.values()){
			studyIds.add(ifr.Study__c);
		}

		SYSTEM.DEBUG('- - - - - - - - - - - SPECIAL FACILITY COUT CALLED - - - - - - - - - - ');

		List<Imaging_Facility__c> imageFacilitiesList = [SELECT Id, Study__c, Supplied__c, Status__c FROM Imaging_Facility__c WHERE Study__c IN : studyIds ];
		List<Study__c> studiesToUpdate = [SELECT Id, Image_Facilities_Supplied__c, Image_Facilities_Not_Supplied__c, Image_Facilities_Inactive__c FROM Study__c WHERE Id IN : studyIds];

		for(Imaging_Facility__c ifr : imageFacilitiesList){
			Integer supplied = 0;
			Integer notSupplied = 0;
			Integer inactive = 0;
			if(ifr.Study__c != null){
				if(studyMapToSupplied.get(ifr.Study__c) != null){
					supplied = studyMapToSupplied.get(ifr.Study__c);
				}
				if(studyMapToNotSupplied.get(ifr.Study__c) != null){
					notSupplied = studyMapToNotSupplied.get(ifr.Study__c);
				}
				if(studyMapToInactive.get(ifr.Study__c) != null){
					inactive = studyMapToInactive.get(ifr.Study__c);
				}
			}

			if(ifr.Status__c == null || ifr.Status__c == 'Active'){
				if(ifr.Supplied__c== null || ifr.Supplied__c == 'No'){
					notSupplied++;
				}
				else{
					supplied++;
				}
			}
			else{
				inactive++;
			}
			studyMapToSupplied.put(ifr.Study__c, supplied);
			studyMapToNotSupplied.put(ifr.Study__c, notSupplied);
			studyMapToInactive.put(ifr.Study__c, inactive);
		}

		for(Study__c s : studiesToUpdate){
			s.Image_Facilities_Supplied__c = studyMapToSupplied.get(s.Id);
			s.Image_Facilities_Not_Supplied__c = studyMapToNotSupplied.get(s.Id);
			s.Image_Facilities_Inactive__c = studyMapToInactive.get(s.Id);
		}

		update studiesToUpdate;
	
		Global_ApexStaticController.Imaging_Facility_Trigger_Off = true;	

	}

	public static void alignImgFacContactsWithStudySiteContacts(List<Imaging_Facility__c> inputIfList){
		Set<Id> studySiteIdSet = new Set<Id>();
		for(Imaging_Facility__c inputIfr : inputIfLIst){
			studySiteIdSet.add(inputIfr.Study_Site__c);
		}
		List<Imaging_Facility__c> imgFacList = [SELECT Id, Study_Site__c, Study_Site_SC__c, Study_Site_PI__c, Study_Site__r.Study_Coordinator__c, Study_Site__r.Principal_Investigator__c FROM Imaging_Facility__c WHERE Study_Site__c IN : studySiteIdSet];
		Boolean updateRecord = false;
		List<Imaging_Facility__c> imgFacsToUpdate = new List<Imaging_Facility__c>();
		for(Imaging_Facility__c ifc : imgFacList){
			if(ifc.Study_Site_SC__c == null && ifc.Study_Site__r.Study_Coordinator__c != null){
				ifc.Study_Site_SC__c = ifc.Study_Site__r.Study_Coordinator__c;
				updateRecord = true;
			}
			else if (ifc.Study_Site__r.Study_Coordinator__c == null && ifc.Study_Site_SC__c != null){
				ifc.Study_Site_SC__c = null;
				updateRecord = true;
			}
			else if(ifc.Study_Site__r.Study_Coordinator__c != ifc.Study_Site_SC__c){
				ifc.Study_SIte_SC__c = ifc.Study_Site__r.Study_Coordinator__c;
				updateRecord = true;
			}

			if(ifc.Study_Site_PI__c == null && ifc.Study_Site__r.Principal_Investigator__c != null){
				ifc.Study_Site_PI__c = ifc.Study_Site__r.Principal_Investigator__c;
				updateRecord = true;
			}
			else if (ifc.Study_Site__r.Principal_Investigator__c == null && ifc.Study_Site_PI__c != null){
				ifc.Study_Site_PI__c = null;
				updateRecord = true;
			}
			else if(ifc.Study_Site__r.Principal_Investigator__c != ifc.Study_Site_PI__c){
				ifc.Study_SIte_PI__c = ifc.Study_Site__r.Principal_Investigator__c;
				updateRecord = true;
			}

			if(updateRecord){
				imgFacsToUpdate.add(ifc);
			}
		}
		update imgFacsToUpdate;

	}
	
	 /******************************************************************************************
        Purpose     : Helper method for Update Imaging facility Trigger. 
        			  SAL- 326 : Remove Shared Facility
        			  SAL- 325 : Change Shared Facility
        Parameters  : imagFacOldMap,imgFacNewMap
        Returns     : Updates oldImagingfacility Record as null
    ******************************************************************************************/
	public static void beforeUpdateSharedImgFacility(Map<Id, Imaging_Facility__c> imagFacOldMap, List<Imaging_Facility__c> imgFacNewMap) {
		system.debug('beforeUpdateSharedImgFacility Starts here -->' +imagFacOldMap+ 'imgFacNewMap --> ' +imgFacNewMap);
		set<Id> oldImgFacID = new set<Id>(); //get all teh old imaging facility shared ID
		for(Imaging_Facility__c img : imgFacNewMap){
			Imaging_Facility__c oldImgFacObj = imagFacOldMap.get(img.id);
			//Assigns only old shared IF != new Shared IF
			if(img.Shared_Imaging_Facility__c !=null && img.Shared_Imaging_Facility__c != oldImgFacObj.Shared_Imaging_Facility__c){
				oldImgFacID.add(oldImgFacObj.Shared_Imaging_Facility__c);
			}else if( oldImgFacObj.Shared_Imaging_Facility__c != null && oldImgFacObj.Shared__c && !img.Shared__c){
				//Assigns only old shared IF != null and and uncheck on shared___C
				oldImgFacID.add(oldImgFacObj.Shared_Imaging_Facility__c);
			}
		}
		system.debug('oldImgFacID -->#'+oldImgFacID);
		//IF records oldImgFacID is not empty
		if(!oldImgFacID.isempty()){ 
			List<Imaging_Facility__c> ImgFacUpdateList = new List<Imaging_Facility__c>();
			//Query wrt oldImgFacID and check any other record exist for old Primary IF apart from curent IF (i.e to check whether current IF is a last secondary record for the Old Primary IF)
			// if Exists no change else update old primary record as unshared because old Primary record doesnt conatins any other secondary record
			List<Imaging_Facility__c> ImgFacObjList =  [SELECT ID , name, Shared_Type__c, Start_up_Comments__c , Shared__c , ( Select Id From Imaging_Facilities__r)  FROM Imaging_Facility__c  where id in: oldImgFacID];
			system.debug('ImgFacObjList -->'+ImgFacObjList);
			for(Imaging_Facility__c img : ImgFacObjList){
				if((img.Imaging_Facilities__r !=null  && img.Imaging_Facilities__r.size()<2) || img.Imaging_Facilities__r ==null){
					Imaging_Facility__c oImg = new Imaging_Facility__c();
					oImg.id = img.id;
					oImg.Shared_Type__c = null;
					oImg.Shared__c = false;
					ImgFacUpdateList.add(oImg);
				}
			}
			system.debug('ImgFacUpdateList --> ' +ImgFacUpdateList);
			if(!ImgFacUpdateList.isempty()){
				update ImgFacUpdateList;
			}
		}
		system.debug('beforeUpdateSharedImgFacility Ends here -->' );
	}
	
}