trigger Study_Team_Trigger on Study_Team__c (after insert, after update, after delete) {

    // AFTER
    if(Trigger.isAfter){

        // AFTER INSERT
        if(Trigger.isInsert){
            Study_Team_TriggerHandler.sth_Insert(Trigger.new, 'Inserted');
        }
        // AFTER UPDATE
        else if(Trigger.isUpdate){
            Study_Team_TriggerHandler.sth_Update(Trigger.oldMap, Trigger.newMap);
        }
        //AFTER DELETE
        else if(Trigger.isDelete){
            Study_Team_TriggerHandler.sth_Delete(Trigger.old);
        }

    }

}



/***************************************************************************************************************
trigger trSTH_delete on Study_Team__c (after delete) {

 List<Study_Team_History__c> new_sth_ins = new List<Study_Team_History__c>();
 Study_Team_History__c sth;

 for ( Study_Team__c st : trigger.old )
 {
   sth = new Study_Team_History__c();     
   if (st.Contact__c != null)
   {
     Study__c s;
     Contact c = [SELECT Name FROM Contact WHERE id = :st.Contact__c];
     sth.Contact__c   = c.Name;

     //update the study record
     if      (st.Role__c == 'SCI - Scientist')
     {
        s = [SELECT Id FROM Study__c WHERE id = :st.study__c];
        s.Scientist__c = null;
        update s;
     }
     else if (st.Role__c == 'BDE - Business Development Executive')
     {
        s = [SELECT Id FROM Study__c WHERE id = :st.study__c];
        s.Business_Development_Executive__c = null;
        update s;
     }
     else if (st.Role__c == 'CPS - Clinical Project Management Supervisor')
     {
        s = [SELECT Id FROM Study__c WHERE id = :st.study__c];
        s.CPM_Supervisor__c = null;
        update s;
     }
   
     sth.Study__c     = st.study__c;
     sth.Role__c      = st.Role__c;
     sth.primary__c   = st.primary__c;
     sth.modality__c  = st.modality__c;
     sth.Mode__c      = 'Deleted';
     sth.CreatedBy__c = st.LastModifiedById;
     sth.CreatedAt__c = st.LastModifiedDate;
     new_sth_ins.add(sth);
   }

 }

 insert new_sth_ins;

}

***************************************************************************************************************

trigger trSTH_insert on Study_Team__c (after insert) {
 
 List<Study_Team_History__c> new_sth_ins = new List<Study_Team_History__c>();
 Study_Team_History__c sth;

 for ( Study_Team__c st : trigger.new )
 {
   sth = new Study_Team_History__c();     
   if (st.Contact__c != null)
   {
     Study__c s;
     Contact c = [SELECT Name,User__r.Id FROM Contact WHERE id = :st.Contact__c];
     sth.Contact__c   = c.Name;

     //update the study record
     if      (st.Role__c == 'SCI - Scientist')
     {
        s = [SELECT Id FROM Study__c WHERE id = :st.study__c];
        s.Scientist__c = c.User__r.Id;
        update s;
     }
     else if (st.Role__c == 'BDE - Business Development Executive')
     {
        s = [SELECT Id FROM Study__c WHERE id = :st.study__c];
        s.Business_Development_Executive__c = c.User__r.Id;
        update s;
     }
     else if (st.Role__c == 'CPS - Clinical Project Management Supervisor')
     {
        s = [SELECT Id FROM Study__c WHERE id = :st.study__c];
        s.CPM_Supervisor__c = c.User__r.Id;
        update s;
     }

     sth.Study__c     = st.study__c;
     sth.Role__c      = st.Role__c;
     sth.primary__c   = st.primary__c;
     sth.modality__c  = st.modality__c;
     sth.Mode__c      = 'Inserted';
     sth.CreatedBy__c = st.CreatedById;
     sth.CreatedAt__c = st.CreatedDate;
     new_sth_ins.add(sth);
   }


 }

 insert new_sth_ins;
}

***************************************************************************************************************

trigger trSTH_update on Study_Team__c (after update) {

 List<Study_Team_History__c> new_sth_ins = new List<Study_Team_History__c>();
 Study_Team_History__c sth_old;
 Study_Team_History__c sth_new;

 for ( Study_Team__c st : trigger.old )
 {

   if ( trigger.newMap.get(st.Id).Contact__c  != st.Contact__c  ||
        trigger.newMap.get(st.Id).Role__c     != st.Role__c     ||
        trigger.newMap.get(st.Id).primary__c  != st.primary__c  ||
        trigger.newMap.get(st.Id).modality__c != st.modality__c 
      )
   {
    Contact c;
    
    sth_old = new Study_Team_History__c();     
    if (st.Contact__c != null)
    {
      c = [SELECT Name FROM Contact WHERE id = :st.Contact__c];
      sth_old.Contact__c  = c.Name;
      sth_old.Study__c     = st.study__c;
      sth_old.Role__c      = st.Role__c;
      sth_old.primary__c   = st.primary__c;
      sth_old.modality__c  = st.modality__c;
      sth_old.Mode__c      = 'deleted';
      sth_old.CreatedBy__c = st.LastModifiedById;
      sth_old.CreatedAt__c = st.LastModifiedDate;
      new_sth_ins.add(sth_old);
    }  

    sth_new = new Study_Team_History__c();     
    if (trigger.newMap.get(st.Id).Contact__c != null)
    {
      Study__c s;
      c = [SELECT Name,User__r.Id FROM Contact WHERE id = :trigger.newMap.get(st.Id).Contact__c];
      sth_new.Contact__c  = c.Name;
    
      //update the study record
      if      (st.Role__c == 'SCI - Scientist')
      {
        s = [SELECT Id FROM Study__c WHERE id = :st.study__c];
        s.Scientist__c = c.User__r.Id;
        update s;
      }
      else if (st.Role__c == 'BDE - Business Development Executive')
      {
        s = [SELECT Id FROM Study__c WHERE id = :st.study__c];
        s.Business_Development_Executive__c = c.User__r.Id;
        update s;
      }
      else if (st.Role__c == 'CPS - Clinical Project Management Supervisor')
      {
        s = [SELECT Id FROM Study__c WHERE id = :st.study__c];
        s.CPM_Supervisor__c = c.User__r.Id;
        update s;
      }
      sth_new.Study__c     = trigger.newMap.get(st.Id).study__c;
      sth_new.Role__c      = trigger.newMap.get(st.Id).Role__c;
      sth_new.primary__c   = trigger.newMap.get(st.Id).primary__c;
      sth_new.modality__c  = trigger.newMap.get(st.Id).modality__c;
      sth_new.Mode__c      = 'inserted';
      sth_new.CreatedBy__c = trigger.newMap.get(st.Id).LastModifiedById;
      sth_new.CreatedAt__c = trigger.newMap.get(st.Id).LastModifiedDate;
      new_sth_ins.add(sth_new);
    }  
    
   }
 }

 insert new_sth_ins;
}


*/